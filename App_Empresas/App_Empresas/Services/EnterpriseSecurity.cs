﻿using App_Empresas.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App_Empresas.Services
{
    public class EnterpriseSecurity
    {
        public static bool Login(string email, string password)
        {
            using (EmpresaContext entities = new EmpresaContext())
            {
                return entities.Usuarios.Any(user => user.Email.Equals(email, StringComparison.OrdinalIgnoreCase) &&
                                                     user.Password.Equals(password));
            }
        }
    }
}