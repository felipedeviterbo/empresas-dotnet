﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Web.Http.Routing;
using Owin;

[assembly: OwinStartup(typeof(App_Empresas.Startup))]

namespace App_Empresas
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap = { ["apiVersion"] = typeof( ApiVersionRouteConstraint )  }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning();
            config.Routes.MapHttpRoute(
                  name: "DefaultApi",
                  routeTemplate: "api/v{version:apiVersion}/{controller}/{id}",
                  defaults: new { id = RouteParameter.Optional }
             );
            app.UseCors(CorsOptions.AllowAll);

            AtivarGeracaoTokenAcesso(app);

            app.UseWebApi(config);
        }

        private void AtivarGeracaoTokenAcesso(IAppBuilder app)
        {
            var opcoesConfiguracaoToken = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/users/auth/sign_in"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1),
                Provider = new ProviderDeTokensDeAcesso()
            };
            app.UseOAuthAuthorizationServer(opcoesConfiguracaoToken);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
