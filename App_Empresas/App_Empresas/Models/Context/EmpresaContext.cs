﻿using App_Empresas.Models.Entities;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace App_Empresas.Models.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EmpresaContext : DbContext
    {
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<TipoEmpresa> TiposEmpresa { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        public EmpresaContext() : base("Contexto")
        {
        }

    }
}