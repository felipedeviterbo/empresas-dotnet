﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace App_Empresas.Models.Entities
{
    [Table("tipoempresa")]
    public class TipoEmpresa
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}