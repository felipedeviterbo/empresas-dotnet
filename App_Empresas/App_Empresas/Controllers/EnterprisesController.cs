﻿using App_Empresas.Models.Context;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App_Empresas.Controllers
{
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/enterprises")]
    public class EnterprisesController : ApiController
    {
        private EmpresaContext db = new EmpresaContext();

        [Authorize]
        [Route]
        public IHttpActionResult Get()
        {
            var empresas = db.Empresas;
            return Ok(empresas);
        }

        [Authorize]
        [Route]
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Id inválido");
            }

            var empresa = db.Empresas.Find(id);
            if (empresa == null)
                return NotFound();
            return Ok(empresa);
        }

        [Authorize]
        [Route]
        public IHttpActionResult Get(int enterprise_types, string name)
        {
            if (enterprise_types <= 0)
                return BadRequest("Enterprise type is invalid");
            if (string.IsNullOrEmpty(name))
                return BadRequest("Name is empty or invalid");

            var empresa = db.Empresas.Where(e => e.RazaoSocial.StartsWith(name) &&
                                                 e.Tipo == enterprise_types)
                                     .OrderBy(e => e.Id);
            if (empresa == null)
                return NotFound();
            return Ok(empresa);
        }
    }
}
